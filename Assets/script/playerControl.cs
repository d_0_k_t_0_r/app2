﻿using UnityEngine;

public class playerControl : MonoBehaviour
{
    private Vector2 pointStart, pointEnd, vel;
    private float time;
    private bool move;
    private GameObject gg;
    public GameObject mesh;

    private void Start()
    {
        gg = this.gameObject;
    }

    void Update()
    {
        vel =new Vector2(Mathf.Abs(gg.GetComponent<Rigidbody2D>().velocity.x),Mathf.Abs(gg.GetComponent<Rigidbody2D>().velocity.y));
        if (vel.x <= 0.1f && vel.y <= 0.1f) move = true;
        else move = false;

        if (move == true)
        {
            UI.uiManager.move.color = Color.green;
            if (Input.GetMouseButtonDown(0))
            {
                time = 0f;
                pointStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            }
            if (Input.GetMouseButton(0))
            {
                time += Time.deltaTime;
            }
            if (Input.GetMouseButtonUp(0))
            {
                pointEnd = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                addForce();
            }
        }
        else UI.uiManager.move.color = Color.red;

    }
    void addForce()
    {
        Vector2 target = new Vector2(gg.transform.position.x + (pointEnd.x - pointStart.x), gg.transform.position.y + (pointEnd.y - pointStart.y));
        gg.transform.LookAt(target);
        float f = gg.GetComponent<Rigidbody2D>().mass * ((Vector2.Distance(pointStart, pointEnd) / time) / time);
        gg.GetComponent<Rigidbody2D>().AddForce(gg.transform.forward * f * 2);
    }   
}
