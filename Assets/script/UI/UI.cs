﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI : MonoBehaviour
{
    public static UI uiManager;
    public Text lvlTXT;
    public Image move;
    public int lvl;

    private void Start()
    {
        uiManager = this;
        lvlTXT.text = lvl.ToString();
    }

    public void nextLevel()
    {
        gaManager.ga.OnLevelComplete(lvl);
        if(lvl>=5) SceneManager.LoadScene("Level 1");
        else SceneManager.LoadScene("Level " + (lvl + 1).ToString());
    }
    public void reStart()
    {
        SceneManager.LoadScene("Level " + lvl.ToString());
    }
}
