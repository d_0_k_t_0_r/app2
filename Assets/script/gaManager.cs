﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class gaManager : MonoBehaviour
{
    public static gaManager ga;

    private void Awake()
    {
        ga = this;
        DontDestroyOnLoad(this);
    }

    void Start()
    {
        GameAnalytics.Initialize();
    }


    void Update()
    {
        
    }
    public void OnLevelComplete(int _level)
    {
        //GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete,"Level: "+_level);
        GameAnalytics.NewDesignEvent("Level: " + _level.ToString(), _level);
        Debug.Log("Level: " + _level);
    }
}
